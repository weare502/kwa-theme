<?php

// change 'views' directory to 'templates'
Timber::$locations = __DIR__ . '/templates';

class KWASite extends TimberSite {

	function __construct() {
		// Action Hooks //
		add_action( 'after_setup_theme', [ $this, 'after_setup_theme' ] );
		add_action( 'after_setup_theme', [ $this, 'kwa_color_palette' ]);
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
		add_action( 'enqueue_block_assets', [ $this, 'backend_frontend_styles' ] );
		add_action( 'admin_head', [ $this, 'admin_head_css' ] );
		add_action( 'admin_menu', [ $this, 'admin_menu_cleanup'] );
		add_action( 'init', [ $this, 'register_post_types' ] );
		add_action( 'acf/init', [ $this, 'render_custom_acf_blocks' ] );

		// Filter Hooks //
		add_filter( 'timber_context', [ $this, 'add_to_context' ] );
		add_filter( 'gform_enable_field_label_visibility_settings', [ $this, '__return_true' ] );
		add_filter( 'block_categories', [ $this, 'kwa_block_category' ], 99, 1 );

		// Comment Column Removal //
		add_filter( 'manage_edit-page_columns', [ $this, 'disable_admin_columns' ] );

		parent::__construct();
	}

	// hide admin area annoyances
	function admin_head_css() {
		?><style type="text/css">
			#wp-admin-bar-comments { display: none !important; }
			.update-nag { display: none !important; }
			#menu-posts-seed { margin-top: 1rem !important; }
			#menu-posts-dealer { margin-bottom: 0.5rem !important; }

			/* Hide annoying ManageWP popup */
			.mwp-notice-container { display: none !important; }

			/* Hide the "Most Used" Category - do not target *__panel as it will break the right-side editor panel
			   This only targets the inserter and not all the panels
			*/
			.block-editor-inserter__results > div:first-of-type {
				display: none !important;
			}

			/* modify our custom seed_order column to have text align center
			   id is for thead, class is for tbody */
			#seed_order, .seed_order { text-align: center; }
		</style><?php
	}

	function enqueue_scripts() {
		$version = '21000024';
		wp_enqueue_style( 'kwa-css', get_stylesheet_directory_uri() . '/style-dist.css', [], $version );
		wp_enqueue_script( 'kwa-js', get_template_directory_uri() . '/static/js/site-dist.js', ['jquery'], $version );
	}

	// Uses the 'enqueue_block_assets' hook
	function backend_frontend_styles() {
		wp_enqueue_style( 'blocks-css', get_stylesheet_directory_uri() . '/block-style-dist.css' );
	}

	// Custom Timber context helper functions
	function add_to_context( $context ) {
		$context['site'] = $this;
		$context['date'] = date('F j, Y');
		$context['date_year'] = date('Y');
		$context['options'] = get_fields('option');
		$context['is_home'] = is_home();
		$context['home_url'] = home_url('/');
		$context['is_front_page'] = is_front_page();
		$context['get_url'] = $_SERVER['REQUEST_URI'];

		return $context;
	}

	// Menus / Theme Support / ACF Options Page
	function after_setup_theme() {
		register_nav_menu( 'primary', 'Site Navigation' );

		add_theme_support( 'menus' );
		add_theme_support( 'align-wide' );
		add_theme_support( 'post-thumbnails' );

		if( function_exists('acf_add_options_page') ) {
			acf_add_options_page([
				'page_title' => 'Global Site Data',
				'menu_title' => 'Global Site Data',
				'capability' => 'edit_posts',
				'redirect' => false,
				'updated_message' => 'Global Options Updated!'
			]);
		}
	}

	// registers and renders our custom acf blocks
	function render_custom_acf_blocks() {
		require 'acf-block-functions.php';
	}

	// custom category for ACF blocks / put category on top of Block Inserter
	function kwa_block_category( $categories ) {
		// setup category array
		$kwa_category = [
			'slug' => 'kwa-blocks',
			'title' => 'KWA Blocks'
		];

		// make a new category array and insert ours at position 1
		$new_categories = [];
		$new_categories[0] = $kwa_category;

		// rebuild cats array
		foreach( $categories as $category ) {
			$new_categories[] = $category;
		}

		return $new_categories;
	}

	// get rid of clutter
	function disable_admin_columns( $columns ) {
		unset( $columns['comments'] );
		return $columns;
	}

	// more de-cluttering
	function admin_menu_cleanup() {
		remove_menu_page( 'edit.php' ); // Posts
		remove_menu_page( 'edit-comments.php' ); // Comments
	}

	// add cpts here
	function register_post_types() {
		include_once('custom-post-types/post-type-seed.php');
		include_once('custom-post-types/post-type-newsletter.php');
		include_once('custom-post-types/post-type-dealer.php');
	}

	// custom color palette
	function kwa_color_palette() {
		add_theme_support( 'disable-custom-colors' );
		$colors = [
			[
				// Purple
				'name' => __( 'Purple', 'kwa' ),
				'slug' => 'purple',
				'color' => '#4A2B74'
			],

			[
				// Light Black
				'name' => __( 'Light Black', 'kwa' ),
				'slug' => 'grey',
				'color' => '#2F2F2F'
			]
		];
		add_theme_support( 'editor-color-palette', $colors );
	}
} // End of KWASite class

new KWASite();

// main site nav
function kwa_render_primary_menu() {
	wp_nav_menu([
		'theme_location' => 'primary',
		'container' => false,
		'menu_id' => 'primary-menu'
	]);
}

// run if  _wp_page_template  is not empty (custom template is used)
// for the Default Template it will be empty. (default is used when no template is set)
// Code Courtesy of: Bill Erickson
function ea_disable_editor( $id = false ) {
	$excluded_templates = [
		'front-page.php',
		'archive-seed.php',
		'archive-news.php'
	];

	if( empty( $id ) )
		return false;

	$id = intval( $id );
	$template = get_page_template_slug( $id );

	return in_array( $template, $excluded_templates );
}

function ea_disable_gutenberg( $can_edit, $post_type ) {
	if( ! ( is_admin() && !empty( $_GET['post'] ) ) )
		return $can_edit;

	if( ea_disable_editor( $_GET['post'] ) )
		$can_edit = false;

	return $can_edit;
}
add_filter( 'gutenberg_can_edit_post_type', 'ea_disable_gutenberg', 10, 2 );
add_filter( 'use_block_editor_for_post_type', 'ea_disable_gutenberg', 10, 2 );

// move our ACF Options Page (Global Site Data) below the Dashboard tab
function custom_menu_order( $menu_ord ) {  
    if ( ! $menu_ord ) {
		return true;
	}

    $menu = 'acf-options-global-site-data';

    // remove from current menu
    $menu_ord = array_diff( $menu_ord, [$menu] );

    // append after index.php [0]
    array_splice( $menu_ord, 1, 0, [$menu] );

    return $menu_ord;
}  
add_filter( 'custom_menu_order', 'custom_menu_order' );
add_filter( 'menu_order', 'custom_menu_order' );

// change default sort for newsletters to be by date in dashboard
function kwa_post_types_admin_order( $wp_query ) {
	if( is_admin() ) {
		$post_type = $wp_query->query['post_type'];

		if( $post_type == 'newsletter') {
			$wp_query->set('orderby', 'date');
			$wp_query->set('order', 'DESC');
	  	}
	}
}
add_filter('pre_get_posts', 'kwa_post_types_admin_order');

// sets "News" as the default newsletter term on save
function set_default_newsletter_term( $post_id, $post ) {
	$slug = 'newsletter';

	// don't run if it's not the newsletter post type
	if( $slug != $post->post_type ) {
		return;
	}

	// get the term's data (for getting term_id)
	$default_term = get_term_by( 'slug', 'news', 'news-type' );

	// is the post has a term already, do not overwrite it
	if( has_term( ['news', 'research'], 'news-type', $post_id ) ) {
		return;
	} else {
		// if no term is set, set "News" as the default term
		wp_set_object_terms( $post_id, $default_term->term_id, 'news-type' );
	}
}
add_action( 'save_post', 'set_default_newsletter_term', 10, 3 );

// add a custom admin column for the seed order on each seed to make changing them easier to see
function add_acf_custom_column( $columns ) {
	return array_merge( $columns, [
		'seed_order' => __( 'Seed Order' ),
	]);
}
add_filter( 'manage_seed_posts_columns', 'add_acf_custom_column' );

// Add the Seed Order column to the Seed post (easier to see what order they are in)
function custom_column_data( $column, $post_id ) {
	switch( $column ) {
		case 'seed_order':
			echo get_post_meta( $post_id, 'seed_order', true );
		break;
	}
}
add_action( 'manage_seed_posts_custom_column', 'custom_column_data', 10, 2 );