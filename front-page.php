<?php
/**
 * Template Name: Home
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['newsletters'] = Timber::get_posts([
	'post_type' => 'newsletter',
	'posts_per_page' => 3,
	'orderby' => 'date',
	'order' => 'DESC',
	'facetwp' => false // prevent facetwp from overriding this query
]);

$templates = [ 'front-page.twig' ];

Timber::render( $templates, $context );