<?php
// This functions file is for all custom blocks added via ACF
// Reference: https://www.advancedcustomfields.com/resources/acf_register_block_type/

if( function_exists('acf_register_block_type') ) :
	include 'acf-blocks-callback.php'; // pass-off to let Timber render the blocks

	/** Blocks **/
	$dropdown_block = [
		'name' => 'dropdown-block',
		'title' => __( 'Dropdown Block', 'kwa' ),
		'description' => __( 'Creates a dropdown container; The content is folded into the dropdown title.', 'kwa' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'kwa-blocks',
		'align' => 'wide',
		'icon' => 'sort',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'drop', 'down', 'dropdown', 'accordion' ]
	];
	acf_register_block_type( $dropdown_block );

	$important_info_block = [
		'name' => 'important-info-block',
		'title' => __( 'Important Info', 'kwa' ),
		'description' => __( 'Creates an important message "banner" with a purple left border, and text on the right.', 'kwa' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'kwa-blocks',
		'align' => 'center',
		'icon' => 'info',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'important', 'info', 'message', 'purple' ]
	];
	acf_register_block_type( $important_info_block );

	$gallery_block = [
		'name' => 'news-gallery',
		'title' => __( 'News Image Gallery', 'kwa' ),
		'description' => __( 'Creates a 5 column image gallery in a grid.\n Clicking an image will open a modal, where the image can be viewed at its normal size.', 'kwa' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'kwa-blocks',
		'align' => 'center',
		'icon' => 'images-alt',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'gallery', 'image', 'grid', 'modal' ]
	];
	acf_register_block_type( $gallery_block );

	$green_button_block = [
		'name' => 'green-button-block',
		'title' => __( 'Green Button', 'kwa' ),
		'description' => __( 'Creates a Green button with white text.', 'kwa' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'kwa-blocks',
		'align' => 'wide',
		'icon' => 'editor-removeformatting',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'button', 'anchor', 'green' ]
	];
	acf_register_block_type( $green_button_block );

	$purple_button_block = [
		'name' => 'purple-button-block',
		'title' => __( 'Purple Button', 'kwa' ),
		'description' => __( 'Creates a Purple button with white text.', 'kwa' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'kwa-blocks',
		'align' => 'wide',
		'icon' => 'editor-removeformatting',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'button', 'anchor', 'purple' ]
	];
	acf_register_block_type( $purple_button_block );
endif;