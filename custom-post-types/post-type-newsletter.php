<?php
// setup the post type
$labels = [
	'name'               => __( 'News', 'kwa' ),
	'singular_name'      => __( 'Newsletter', 'kwa' ),
	'add_new'            => _x( 'Add News', 'kwa', 'kwa' ),
	'add_new_item'       => __( 'Add News', 'kwa' ),
	'edit_item'          => __( 'Edit News', 'kwa' ),
	'new_item'           => __( 'New News', 'kwa' ),
	'view_item'          => __( 'View News', 'kwa' ),
	'search_items'       => __( 'Search News', 'kwa' ),
	'not_found'          => __( 'No News found', 'kwa' ),
	'not_found_in_trash' => __( 'No News found in Trash', 'kwa' ),
	'parent_item_colon'  => __( 'Parent News:', 'kwa' ),
	'menu_name'          => __( 'News', 'kwa' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => true,
	'description'         => '',
	'taxonomies'          => [ 'news-type' ],
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_icon'           => 'dashicons-format-aside',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true, // main news listing
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'editor', 'title', 'thumbnail' ],
];
register_post_type( 'newsletter', $args );

// setup taxonomies
$news_tax_labels = [
	'name' 				=> _x( 'News Types', 'kwa' ),
	'singular_name' 	=> _x( 'News Type', 'kwa' ),
	'search_items' 		=> __( 'Search News Types', 'kwa' ),
	'all_items' 		=> __( 'All News Types', 'kwa' ),
	'edit_item' 		=> __( 'Edit News Type', 'kwa' ),
	'update_item' 		=> __( 'Update News Type', 'kwa' ),
	'add_new_item' 		=> __( 'Add News Type', 'kwa' ),
	'new_item_name' 	=> __( 'Create News Type', 'kwa' ),
	'menu_name' 		=> __( 'News Types', 'kwa' ),
	'parent_item'		=> NULL,
];

$news_tax_args = [
	'hierarchical' 	    => true,
	'labels' 	    	=> $news_tax_labels,
	'show_ui' 	    	=> true,
	'show_admin_column' => true,
	'has_archive'		=> false,
	'query_var'	    	=> true,
	'show_in_rest'		=> true,
	'rewrite'			=> true,
];
register_taxonomy( 'news-type', 'newsletter', $news_tax_args );