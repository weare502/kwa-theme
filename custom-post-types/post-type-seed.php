<?php
// Setup the post type
$labels = [
	'name'               => __( 'Seeds', 'kwa' ),
	'singular_name'      => __( 'Seed', 'kwa' ),
	'add_new'            => _x( 'Add Seed', 'kwa', 'kwa' ),
	'add_new_item'       => __( 'Add Seed', 'kwa' ),
	'edit_item'          => __( 'Edit Seed', 'kwa' ),
	'new_item'           => __( 'New Seed', 'kwa' ),
	'view_item'          => __( 'View Seed', 'kwa' ),
	'search_items'       => __( 'Search Seeds', 'kwa' ),
	'not_found'          => __( 'No Seeds found', 'kwa' ),
	'not_found_in_trash' => __( 'No Seeds found in Trash', 'kwa' ),
	'parent_item_colon'  => __( 'Parent Seed:', 'kwa' ),
	'menu_name'          => __( 'Seeds', 'kwa' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => true,
	'description'         => '',
	'taxonomies'          => [ 'seed-class', 'region' ],
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => false,
	'menu_icon'           => 'dashicons-carrot',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true, // main seed listing
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'editor', 'title', 'thumbnail' ],
];
register_post_type( 'seed', $args );

// setup taxonomies
$taxonomies = [
	[
		'slug' => 'seed-class',
		'single_name' => 'Seed Class',
		'plural_name' => 'Seed Classes',
		'post_type' => 'seed',
	],

	[
		'slug' => 'region',
		'single_name' => 'Region',
		'plural_name' => 'Regions',
		'post_type' => 'seed',
	]
];

// register taxonomies
foreach( $taxonomies as $tax ) :
	$tax_labels = [
		'name' 				=> $tax['plural_name'],
		'singular_name' 	=> $tax['single_name'],
		'search_items' 		=> 'Search ' . $tax['plural_name'],
		'all_items' 		=> 'All ' . $tax['plural_name'],
		'edit_item' 		=> 'Edit ' . $tax['single_name'],
		'update_item' 		=> 'Update ' . $tax['single_name'],
		'add_new_item' 		=> 'Add New ' . $tax['single_name'],
		'new_item_name' 	=> 'New ' . $tax['single_name'],
		'menu_name' 		=> $tax['plural_name'],
		'parent_item'		=> NULL, // remove parent items to prevent breaking if someone adds one
	];

	$rewrite = [ 'slug' => $tax['slug'] ];
	$hierarchical = true;

	$tax_args = [
		'hierarchical' 	    => $hierarchical,
		'labels' 	    	=> $tax_labels,
		'show_ui' 	    	=> true,
		'show_admin_column' => true,
		'has_archive'		=> false,
		'query_var'	    	=> true,
		'show_in_rest'		=> true, // required to show categories in Block Editor sidebar
		'rewrite'			=> $rewrite,
	];
	register_taxonomy( $tax['slug'], $tax['post_type'], $tax_args );
endforeach;