<?php
// setup the post type
$labels = [
	'name'               => __( 'Dealers', 'kwa' ),
	'singular_name'      => __( 'Dealer', 'kwa' ),
	'add_new'            => _x( 'Add Dealer', 'kwa', 'kwa' ),
	'add_new_item'       => __( 'Add Dealer', 'kwa' ),
	'edit_item'          => __( 'Edit Dealer', 'kwa' ),
	'new_item'           => __( 'New Dealer', 'kwa' ),
	'view_item'          => __( 'View Dealer', 'kwa' ),
	'search_items'       => __( 'Search Dealers', 'kwa' ),
	'not_found'          => __( 'No Dealers found', 'kwa' ),
	'not_found_in_trash' => __( 'No Dealers found in Trash', 'kwa' ),
	'parent_item_colon'  => __( 'Parent Dealer:', 'kwa' ),
	'menu_name'          => __( 'Dealers', 'kwa' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => true,
	'description'         => '',
	'taxonomies'          => [ 'dealer-region', 'seed-sold' ],
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_icon'           => 'dashicons-groups',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'title', 'thumbnail' ],
];
register_post_type( 'dealer', $args );

// setup taxonomies
$dealer_taxonomies = [
	[
		'slug' => 'dealer-region',
		'single_name' => 'Dealer Region',
		'plural_name' => 'Dealer Regions',
		'post_type' => 'dealer',
	],

	[
		'slug' => 'seed-sold',
		'single_name' => 'Seed Sold',
		'plural_name' => 'Seeds Sold',
		'post_type' => 'dealer',
	]
];

// register taxonomies
foreach( $dealer_taxonomies as $tax ) :
	$tax_labels = [
		'name' 				=> $tax['plural_name'],
		'singular_name' 	=> $tax['single_name'],
		'search_items' 		=> 'Search ' . $tax['plural_name'],
		'all_items' 		=> 'All ' . $tax['plural_name'],
		'edit_item' 		=> 'Edit ' . $tax['single_name'],
		'update_item' 		=> 'Update ' . $tax['single_name'],
		'add_new_item' 		=> 'Add New ' . $tax['single_name'],
		'new_item_name' 	=> 'New ' . $tax['single_name'],
		'menu_name' 		=> $tax['plural_name'],
		'parent_item'		=> NULL, // remove parent items to prevent breaking if someone adds one
	];

	$rewrite = [ 'slug' => $tax['slug'] ];
	$hierarchical = true;

	$tax_args = [
		'hierarchical' 	    => $hierarchical,
		'labels' 	    	=> $tax_labels,
		'show_ui' 	    	=> true,
		'show_admin_column' => true,
		'has_archive'		=> false,
		'query_var'	    	=> true,
		'show_in_rest'		=> true, // required to show categories in Block Editor sidebar
		'rewrite'			=> $rewrite,
	];
	register_taxonomy( $tax['slug'], $tax['post_type'], $tax_args );
endforeach;