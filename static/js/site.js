(function($) {
    $(document).ready(function() {
		let $body = $('body');

		// If a query has been made on the homepage using the selects, and then the back button is used, the homepage breaks slightly
		// we use this to ensure there are no accessibility issues or navigation issues if navigating back after making a query
		if( $body.hasClass('page-template-front-page') ) {
			$(function resetQuery() {
				var uri = window.location.toString(); // stringify URL

				// check if we have a ? in the url (FWP query has been set)
				if( uri.indexOf('?') > 0 ) {
					var clean_uri = uri.substring(0, uri.indexOf('?'));
					window.history.replaceState({}, document.title, clean_uri);
					FWP.reset(); // clear the query
				}
			});
		}

		// mobile menu open/close functions
		$(function siteNavigation() {
			$('#menu-toggle').click(function(e) {
				e.preventDefault(); // prevents scrollTop()
				$('.x-bar').toggleClass('x-bar-active');
				$('#primary-menu').slideToggle(550);
			});

			// make sure we are on mobile since we are targeting classes
			// dropdown function for mobile menu
			if( $body.width() < 850 ) {
				$('.menu-item-has-children').click(function() {
					var $sub = $(this).find('.sub-menu:first');
					$sub.slideToggle(550);
				});
			}
		});

		// dropdown block - controls, events, and aria swap
		$(function dropDownBlock() {
			$('.dropdown-content').each(function() {
				$(this).hide();
			});

			$('.dropdown-block-title').click(function() {
				var $this = $(this);
				// fires on first click (content is expanded)
				if( $this.hasClass('target') ) {
					$this.removeClass('target');
					$this.next('.dropdown-content').slideToggle(700);
					$this.attr('aria-pressed', 'true');
					$this.next('.dropdown-content').attr('aria-expanded', 'true');
				} else {
					// fires on second click (content is closed)
					$this.next('.dropdown-content:first').slideToggle(700, function() {
						$this.prev('.dropdown-block-title').addClass('target');
						$this.prev('.dropdown-block-title').attr('aria-pressed', 'false');
						$this.attr('aria-expanded', 'false');
					});
				}
				// always fire
				$(this).toggleClass('chevron-rotate');
			});
		});

		// Newsletter button filters
		if( $body.hasClass('post-type-archive-newsletter') ) {
			$(function filterNews() {
				// Newsletter <article>
				var news = $('article.News');
				var research = $('article.Research');

				// Buttons active states
				var btn_news = $('.News');
				var btn_research = $('.Research');

				// set hash on this page to blank on initial load (or when page is refreshed)
				window.location.hash = "";

				$(window).on('hashchange', function(e) {
					if( location.hash === '#News' ) {
						btn_news.addClass('active');
						btn_research.removeClass('active');
						news.show();
						research.hide();
					} else if( location.hash === '#Research' ) {
						btn_news.removeClass('active');
						btn_research.addClass('active');
						news.hide();
						research.show();
					}
				});
			}); // end news filter function
		} // end of body class check (post-type-archive-newsletter)

		// only target single seed page anchors and policies page -> for jump list
		if( $body.hasClass('single-seed') || $body.hasClass('page-id-331') ) {
			$(function smoothScroll() {
				$('a[href*="#"]')
				.not('[href="#"]')
				.not('[href="#0"]')
				.click(function(event) {
					if( location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname ) {
						var target = $(this.hash);
						target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

						if(target.length) {
							// Only prevent default if animation is actually gonna happen
							event.preventDefault();
							$('html, body').animate({
								scrollTop: target.offset().top
							}, 1000, function() {

								var $target = $(target);
								$target.focus();
								if ($target.is(":focus")) {
									return false;
								} else {
									$target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
									$target.focus();
								}
							});
						}
					}
				});
			});
		} // end of $body class check (single-seed)

		// create a popup for the varieties chart (full size)
		if( $body.hasClass('page-id-664') && $body.width() > 480 ) {
			var $img_src = $('.full-width-image > img').attr('src');
			$('.image-modal').hide();

			$('.full-width-image').click(function() {
				$('#src-set').attr('src', $img_src);
				$('.image-modal').show();
			});

			// close if the modal is clicked
			$('.image-modal').click(function() {
				$(this).hide();
			});
		} else {
			$('.image-modal').hide();
		}

	}); // end Document.Ready

	// Facet Reset Button (both seed pages utilize same template)
	$(document).on('facetwp-loaded', function() {
		var qs = FWP.build_query_string();
		if ( '' === qs ) { // no facets are selected
			$('.reset-button').hide();
		} else {
			$('.reset-button').show();
		}

		// hide dealer post type initially on load (only applied to dealers - other posts are visible)
		// when the user chooses a dealer facet value, load the dealer grid(s)
		if( FWP.loaded ) {
			$('.facetwp-template').addClass('visible');
		}

		$.each(FWP.settings.num_choices, function(key, val) {
            var $parent = $('.facetwp-facet-' + key).closest('.facet-filter-group');
			if(0 === val) {
				$parent.hide();
				$parent.prev('span').text('No Dealers sell this seed currently.');
			} else {
				$parent.show();
			}
        });
	});
})(jQuery);