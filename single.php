<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

// insect/disease table setup
$disease_table = get_field('disease_seed_table');
$context['disease_table'] = $disease_table;

// agronomic characteristics table setup
$ag_table = get_field('agronomic_seed_table');
$context['agronomic_table'] = $ag_table;

// test weights table setup (Changed to: Regional Yield Performance)
$weights_table = get_field('test_weights_table');
$context['weights_table'] = $weights_table;

/**
 * In the tax_query we only want seeds-sold to match the current seed pages title.
 * This ensures we only show the dealers information on the current seed page, not for all seeds that dealer may sell
*/
$context['dealers'] = Timber::get_posts([
	'post_type' => 'dealer',
	'posts_per_page' => -1,
	'tax_query' => [
		'relation' => 'AND',
		[
			'taxonomy' => 'seed-sold',
			'field' => 'name', // returns the actual name of the seed-sold tax, which is 1:1 with the seeds post type title
			'terms' => $post->title, // the seed-sold term can only match the current post title, returning the matching array of seeds
			'oparator' => 'IN'
		]
	],
	'orderby' => 'title',
	'order' => 'ASC',
	'facetwp' => true
]);

// get the seeds post type to work with in the dealers page
$context['t_seeds'] = Timber::get_posts([
	'post_type' => 'seed',
	'posts_per_page' => -1,
	'order' => 'ASC'
]);

the_post();

Timber::render( [ 'single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig' ], $context );