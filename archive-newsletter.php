<?php
/**
 * Template Name: News Archive
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

// setup posts args
$newsletters = [
	'post_type' => 'newsletter',
	'orderby' => 'date',
	'order' => 'DESC',
	'paged' => $paged
];

// get post terms
$context['news_types'] = Timber::get_terms( ['taxonomies' => 'news-type'] );
$context['posts'] = new Timber\PostQuery( $newsletters );

// setup post query and paging
global $wp_query;
global $paged;
$big = 9999999; // need an unlikely integer

if (!isset($paged) || !$paged){
    $paged = 1;
}

// setup pagination links (uses the default 10 posts per page)
$context['paginate'] = paginate_links( array(
    'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
    'format'  => '?paged=%#%',
    'current' => max( 1, get_query_var('paged') ),
	'total'   => $wp_query->max_num_pages,
	'prev_next' => false,
	'show_all' => true,
) );

$templates = [ 'archive-newsletter.twig' ];

Timber::render( $templates, $context );