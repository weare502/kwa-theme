<?php
/**
 * Template Name: Seed Archive
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$seeds = [
	'post_type' => 'seed',
	'posts_per_page' => -1,
	'meta_key' => 'seed_order',
	'orderby' => 'meta_value_num',
	'order' => 'ASC'
];

$context['seeds'] = Timber::get_posts($seeds);

$templates = [ 'archive-seed.twig' ];

Timber::render( $templates, $context );